﻿using Atoz.Web.Services;
using System.Linq;
using System.Security.Claims;
using System.Web.Mvc;

namespace Atoz.Web.Controllers
{
    public class ProfileController : Controller
    {
        private readonly IProjectsService service;

        public ProfileController(IProjectsService service)
        {
            this.service = service;
        }
        
        public ActionResult MyProjects()
        {
            string userId = GetCurrentUserId();

            var viewModel = service.GetMyProjects(userId);

            return View("Index",viewModel);
        }

        public ActionResult MyResources()
        {
            string userId = GetCurrentUserId();

            var viewModel = service.GetMyResources(userId);

            return View("OfferResource", viewModel);
        }


        private string GetCurrentUserId()
        {
            var userId = string.Empty;
            var claimsIdentity = User.Identity as ClaimsIdentity;
            if (claimsIdentity != null)
            {
                var userIdClaim = claimsIdentity.Claims
                    .FirstOrDefault(x => x.Type == ClaimTypes.NameIdentifier);

                if (userIdClaim != null)
                {
                    userId = userIdClaim.Value;
                }
            }

            return userId;
        }
    }
}