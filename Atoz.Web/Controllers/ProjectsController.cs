﻿using System.Collections.Generic;
using System.Web.Mvc;
using Atoz.Web.Models.DTOs;
using Atoz.Web.Models.ViewModels;
using Atoz.Web.Repositories;
using Atoz.Web.Services;

namespace Atoz.Web.Controllers
{
    public class ProjectsController : Controller
    {
        private readonly IProjectsService service;

        public ProjectsController(IProjectsService service)
        {
            this.service = service;
        }

        [Authorize]
        [HttpGet]
        public ActionResult Create()
        {
            var viewmodel = new ProjectCreateViewModel();
            return this.View("Create", viewmodel);
        }

        [HttpPost]
        [Authorize]
        public ActionResult Create(ProjectCreateDto model)
        {
            if (ModelState.IsValid)
            {
                this.service.CreateProject(model);
                return RedirectToAction("Index", "Projects");
            }

            var viewmodel = new ProjectCreateViewModel { Title = model.Title, Description = model.Description, Tags = model.Tags };
            return this.View("Create", viewmodel);
        }

        public ActionResult Index()
        {
            var viewmodel = this.service.GetProjectsIndexViewModel();
            return this.View("Index", viewmodel);
        }

        public ActionResult Details(int id)
        {
            var viewmodel = this.service.GetProjectDetailsViewModel(id);
            return this.View("Details", viewmodel);
        }

        public ActionResult Edit(object id)
        {
            return RedirectToAction("Index");
        }

        [HttpPost]
        public void Like(int id)
        {
            service.Like(id);
        }

        [HttpPost]
        public void Dislike(int id)
        {
            service.Dislike(id);
        }

        [HttpGet]
        public ActionResult Search()
        {
            var viewmodel = new ProjectsSearchViewModel();
            return this.View("Search", viewmodel);
        }

        [HttpPost]
        public ActionResult Search(HomeDto dto)
        {
            ProjectsSearchViewModel model = service.GetProjectsSearchViewModel(dto);
            return View("Search", model);
        }

    }

    public class ProjectsSearchViewModel
    {
        public ProjectsSearchViewModel()
        {
            this.SearchProjects=new List<ProjectTitle>();
        }
        public string ProjectTags { get; set; }
        public List<ProjectTitle> SearchProjects { get; set; }
    }
}