﻿using Atoz.Web.Models.ViewModels;
using Atoz.Web.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Atoz.Web.Models.DTOs;

namespace Atoz.Web.Controllers
{
    public class HomeController : Controller
    {
        private readonly IProjectsService service;

        public HomeController(IProjectsService service)
        {
            this.service = service;
        }

        [HttpGet]
        public ActionResult Index()
        {
            HomeIndexViewModel model = service.GetHomeIndexViewModel();

            return View("Index", model);
        }
       
        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}