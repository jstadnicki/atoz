﻿using Atoz.Web.Models.DTOs;
using Atoz.Web.Models.ViewModels;
using Atoz.Web.Services;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Atoz.Web.Controllers
{
    public class ResourcesController : Controller
    {
        private readonly IResourcesService service;

        public ResourcesController(IResourcesService service)
        {
            this.service = service;
        }

        [Authorize]
        [HttpGet]
        public ActionResult Create()
        {
            var viewmodel = new ResourceCreateViewModel();
            return this.View("Create", viewmodel);
        }

        [HttpPost]
        public ActionResult Create(ResourceCreateDto model)
        {
            if (ModelState.IsValid)
            {
                this.service.CreateResource(model);
                return RedirectToAction("Index", "Resources");
            }

            var viewModel = Mapper.Map<ResourceCreateViewModel>(model);
            return View("Create", viewModel);
        }

        public ActionResult Index()
        {
            var viewmodel = this.service.GetResourcesIndexViewModel();
            return this.View("Index", viewmodel);
        }

        public ActionResult Details(int id)
        {
            var viewmodel = this.service.GetResourceDetailsViewModel(id);
            return this.View("Details", viewmodel);
        }

        public ActionResult Edit(object id)
        {
            return RedirectToAction("Index");
        }

        //public ActionResult Reserve(int id)
        //{

        //    var viewmodel = this.service.GetResourceDetailsViewModel(id);
        //    return this.View("Details", viewmodel);
        //}
    }
}