﻿using System.Web.Mvc;
using Atoz.Web.Services;

namespace Atoz.Web.Controllers
{
    public class TagsController : Controller
    {
        private readonly ITagsService service;
        private readonly IProjectsService _projectsService;
        private readonly IResourcesService _resourcesService;

        public TagsController(ITagsService service, IProjectsService projectsService, IResourcesService resourcesService)
        {
            this.service = service;
            _projectsService = projectsService;
            _resourcesService = resourcesService;
        }

        [HttpGet]
        public ActionResult Get(string prefix)
        {
            return Json(service.Get(prefix), JsonRequestBehavior.AllowGet);
        }

        public ActionResult Index()
        {
            var model = this.service.GetTop(20);
            return this.View("Index", model);
        }

        public ActionResult DetailsProject(string tag)
        {
            var model = this._projectsService.GetProjectsByTag(tag);
            return this.View("DetailsProject", model);
        }

        public ActionResult DetailsResource(string tag)
        {
            var model = this._resourcesService.GetResourcesByTag(tag);
            return this.View("DetailsResource", model);
        }
    }
}