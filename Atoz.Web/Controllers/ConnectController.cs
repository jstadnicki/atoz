﻿using System.Net;
using System.Web.Mvc;
using Atoz.Web.Services;

namespace Atoz.Web.Controllers
{
    public class ConnectController : Controller
    {
        private readonly IConnectService connectService;

        public ConnectController(IConnectService connectService)
        {
            this.connectService = connectService;
        }

        public ActionResult Sponsor(int r, int s)
        {
            this.connectService.SponsorResource(r, s);
            return new HttpStatusCodeResult(HttpStatusCode.Accepted);
        }

        public ActionResult Request(int s, int r)
        {
            this.connectService.RequestResource(s, r);
            return new HttpStatusCodeResult(HttpStatusCode.Accepted);
        }

        public ActionResult Accept(int r, int s)
        {
            this.connectService.Accept(r, s);
            return new HttpStatusCodeResult(HttpStatusCode.Accepted);
        }
    }
}