namespace Atoz.Web.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addlikedislike : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Dislikes",
                c => new
                    {
                        UserId = c.String(nullable: false, maxLength: 128),
                        ProjectId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.UserId, t.ProjectId })
                .ForeignKey("dbo.Projects", t => t.ProjectId, cascadeDelete: true)
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId)
                .Index(t => t.ProjectId);
            
            CreateTable(
                "dbo.Likes",
                c => new
                    {
                        UserId = c.String(nullable: false, maxLength: 128),
                        ProjectId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.UserId, t.ProjectId })
                .ForeignKey("dbo.Projects", t => t.ProjectId, cascadeDelete: true)
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId)
                .Index(t => t.ProjectId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Likes", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.Likes", "ProjectId", "dbo.Projects");
            DropForeignKey("dbo.Dislikes", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.Dislikes", "ProjectId", "dbo.Projects");
            DropIndex("dbo.Likes", new[] { "ProjectId" });
            DropIndex("dbo.Likes", new[] { "UserId" });
            DropIndex("dbo.Dislikes", new[] { "ProjectId" });
            DropIndex("dbo.Dislikes", new[] { "UserId" });
            DropTable("dbo.Likes");
            DropTable("dbo.Dislikes");
        }
    }
}
