namespace Atoz.Web.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ResourcesMatchtablecreate : DbMigration
    {
        public override void Up()
        {
            RenameTable(name: "dbo.ResourceAtozTags", newName: "AtozTagResources");
            DropPrimaryKey("dbo.AtozTagResources");
            CreateTable(
                "dbo.ResourcesMatches",
                c => new
                    {
                        RequiredResourceId = c.Int(nullable: false),
                        ResourceId = c.Int(nullable: false),
                        Status = c.Int(nullable: false),
                        CreateDate = c.DateTime(nullable: false),
                        AcceptDate = c.DateTime(),
                    })
                .PrimaryKey(t => new { t.RequiredResourceId, t.ResourceId })
                .ForeignKey("dbo.RequiredResources", t => t.RequiredResourceId, cascadeDelete: true)
                .ForeignKey("dbo.Resources", t => t.ResourceId, cascadeDelete: true)
                .Index(t => t.RequiredResourceId)
                .Index(t => t.ResourceId);
            
            AddPrimaryKey("dbo.AtozTagResources", new[] { "AtozTag_Text", "Resource_Id" });
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.ResourcesMatches", "ResourceId", "dbo.Resources");
            DropForeignKey("dbo.ResourcesMatches", "RequiredResourceId", "dbo.RequiredResources");
            DropIndex("dbo.ResourcesMatches", new[] { "ResourceId" });
            DropIndex("dbo.ResourcesMatches", new[] { "RequiredResourceId" });
            DropPrimaryKey("dbo.AtozTagResources");
            DropTable("dbo.ResourcesMatches");
            AddPrimaryKey("dbo.AtozTagResources", new[] { "Resource_Id", "AtozTag_Text" });
            RenameTable(name: "dbo.AtozTagResources", newName: "ResourceAtozTags");
        }
    }
}
