namespace Atoz.Web.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class newprojectsandtags : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Projects",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Title = c.String(),
                        Description = c.String(),
                        UserId = c.String(),
                        Created = c.DateTime(nullable: false),
                        Modified = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.ProjectTags",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Text = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.ProjectTagProjects",
                c => new
                    {
                        ProjectTag_Id = c.Int(nullable: false),
                        Project_Id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.ProjectTag_Id, t.Project_Id })
                .ForeignKey("dbo.ProjectTags", t => t.ProjectTag_Id, cascadeDelete: true)
                .ForeignKey("dbo.Projects", t => t.Project_Id, cascadeDelete: true)
                .Index(t => t.ProjectTag_Id)
                .Index(t => t.Project_Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.ProjectTagProjects", "Project_Id", "dbo.Projects");
            DropForeignKey("dbo.ProjectTagProjects", "ProjectTag_Id", "dbo.ProjectTags");
            DropIndex("dbo.ProjectTagProjects", new[] { "Project_Id" });
            DropIndex("dbo.ProjectTagProjects", new[] { "ProjectTag_Id" });
            DropTable("dbo.ProjectTagProjects");
            DropTable("dbo.ProjectTags");
            DropTable("dbo.Projects");
        }
    }
}
