namespace Atoz.Web.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class newresources : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Resources",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        UserId = c.String(maxLength: 128),
                        Created = c.DateTime(nullable: false),
                        Modified = c.DateTime(nullable: false),
                        Amount = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AspNetUsers", t => t.UserId)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.ResourceAtozTags",
                c => new
                    {
                        Resource_Id = c.Int(nullable: false),
                        AtozTag_Text = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => new { t.Resource_Id, t.AtozTag_Text })
                .ForeignKey("dbo.Resources", t => t.Resource_Id, cascadeDelete: true)
                .ForeignKey("dbo.AtozTags", t => t.AtozTag_Text, cascadeDelete: true)
                .Index(t => t.Resource_Id)
                .Index(t => t.AtozTag_Text);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Resources", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.ResourceAtozTags", "AtozTag_Text", "dbo.AtozTags");
            DropForeignKey("dbo.ResourceAtozTags", "Resource_Id", "dbo.Resources");
            DropIndex("dbo.ResourceAtozTags", new[] { "AtozTag_Text" });
            DropIndex("dbo.ResourceAtozTags", new[] { "Resource_Id" });
            DropIndex("dbo.Resources", new[] { "UserId" });
            DropTable("dbo.ResourceAtozTags");
            DropTable("dbo.Resources");
        }
    }
}
