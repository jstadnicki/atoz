namespace Atoz.Web.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class usernavigation : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Projects", "UserId", c => c.String(maxLength: 128));
            CreateIndex("dbo.Projects", "UserId");
            AddForeignKey("dbo.Projects", "UserId", "dbo.AspNetUsers", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Projects", "UserId", "dbo.AspNetUsers");
            DropIndex("dbo.Projects", new[] { "UserId" });
            AlterColumn("dbo.Projects", "UserId", c => c.String());
        }
    }
}
