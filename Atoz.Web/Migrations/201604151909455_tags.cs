namespace Atoz.Web.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class tags : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.ProjectTagProjects", "ProjectTag_Id", "dbo.ProjectTags");
            DropIndex("dbo.ProjectTagProjects", new[] { "ProjectTag_Id" });
            RenameColumn(table: "dbo.ProjectTagProjects", name: "ProjectTag_Id", newName: "ProjectTag_Text");
            DropPrimaryKey("dbo.ProjectTags");
            DropPrimaryKey("dbo.ProjectTagProjects");
            AlterColumn("dbo.ProjectTags", "Text", c => c.String(nullable: false, maxLength: 128));
            AlterColumn("dbo.ProjectTagProjects", "ProjectTag_Text", c => c.String(nullable: false, maxLength: 128));
            AddPrimaryKey("dbo.ProjectTags", "Text");
            AddPrimaryKey("dbo.ProjectTagProjects", new[] { "ProjectTag_Text", "Project_Id" });
            CreateIndex("dbo.ProjectTagProjects", "ProjectTag_Text");
            AddForeignKey("dbo.ProjectTagProjects", "ProjectTag_Text", "dbo.ProjectTags", "Text", cascadeDelete: true);
            DropColumn("dbo.ProjectTags", "Id");
        }
        
        public override void Down()
        {
            AddColumn("dbo.ProjectTags", "Id", c => c.Int(nullable: false, identity: true));
            DropForeignKey("dbo.ProjectTagProjects", "ProjectTag_Text", "dbo.ProjectTags");
            DropIndex("dbo.ProjectTagProjects", new[] { "ProjectTag_Text" });
            DropPrimaryKey("dbo.ProjectTagProjects");
            DropPrimaryKey("dbo.ProjectTags");
            AlterColumn("dbo.ProjectTagProjects", "ProjectTag_Text", c => c.Int(nullable: false));
            AlterColumn("dbo.ProjectTags", "Text", c => c.String());
            AddPrimaryKey("dbo.ProjectTagProjects", new[] { "ProjectTag_Id", "Project_Id" });
            AddPrimaryKey("dbo.ProjectTags", "Id");
            RenameColumn(table: "dbo.ProjectTagProjects", name: "ProjectTag_Text", newName: "ProjectTag_Id");
            CreateIndex("dbo.ProjectTagProjects", "ProjectTag_Id");
            AddForeignKey("dbo.ProjectTagProjects", "ProjectTag_Id", "dbo.ProjectTags", "Id", cascadeDelete: true);
        }
    }
}
