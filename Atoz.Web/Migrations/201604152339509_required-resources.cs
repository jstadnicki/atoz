namespace Atoz.Web.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class requiredresources : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.RequiredResources",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Description = c.String(),
                        Amount = c.Int(nullable: false),
                        ProjectId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Projects", t => t.ProjectId, cascadeDelete: true)
                .Index(t => t.ProjectId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.RequiredResources", "ProjectId", "dbo.Projects");
            DropIndex("dbo.RequiredResources", new[] { "ProjectId" });
            DropTable("dbo.RequiredResources");
        }
    }
}
