namespace Atoz.Web.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class tagsclassnamechanged : DbMigration
    {
        public override void Up()
        {
            RenameTable(name: "dbo.ProjectTags", newName: "AtozTags");
            RenameTable(name: "dbo.ProjectTagProjects", newName: "AtozTagProjects");
            RenameColumn(table: "dbo.AtozTagProjects", name: "ProjectTag_Text", newName: "AtozTag_Text");
            RenameIndex(table: "dbo.AtozTagProjects", name: "IX_ProjectTag_Text", newName: "IX_AtozTag_Text");
        }
        
        public override void Down()
        {
            RenameIndex(table: "dbo.AtozTagProjects", name: "IX_AtozTag_Text", newName: "IX_ProjectTag_Text");
            RenameColumn(table: "dbo.AtozTagProjects", name: "AtozTag_Text", newName: "ProjectTag_Text");
            RenameTable(name: "dbo.AtozTagProjects", newName: "ProjectTagProjects");
            RenameTable(name: "dbo.AtozTags", newName: "ProjectTags");
        }
    }
}
