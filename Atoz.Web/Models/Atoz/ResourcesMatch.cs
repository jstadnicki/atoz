﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Atoz.Web.Models.Atoz
{
    public class ResourcesMatch
    {
        [Key]
        [Column(Order = 1)]
        public int RequiredResourceId { get; set; }
        [Key]
        [Column(Order = 2)]
        public int ResourceId { get; set; }
        public ResourcesMatchStatus Status { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime? AcceptDate { get; set; }

        public RequiredResource RequiredResource { get; set; }
        public Resource Resource { get; set; }
    }

    public enum ResourcesMatchStatus
    {
        Offered,
        Required,
        Accepted
    }
}