﻿using System;
using System.Collections.Generic;

namespace Atoz.Web.Models.Atoz
{
    public class Project
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string UserId { get; set; }
        public DateTime Created { get; set; }
        public DateTime Modified { get; set; }
        public List<AtozTag> Tags { get; set; }
        public ApplicationUser User { get; set; }
        public List<RequiredResource> RequiredResources { get; set; }
        public List<Like> Likes { get; set; }
        public List<Dislike> Dislikes { get; set; }
    }
}
