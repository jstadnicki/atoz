﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Atoz.Web.Models.Atoz
{
    public class Resource
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string UserId { get; set; }
        public DateTime Created { get; set; }
        public DateTime Modified { get; set; }
        public int Amount { get; set; }

        [ForeignKey("UserId")]
        public ApplicationUser User { get; set; }
        public List<AtozTag> Tags { get; set; }

        public List<ResourcesMatch> RequiredResourceMatches { get; set; }
    }
}