﻿using System.Collections.Generic;
namespace Atoz.Web.Models.Atoz
{
    public class RequiredResource
    {
        public int Id { get; set; }
        public string Description { get; set; }
        public int Amount { get; set; }
        public int ProjectId { get; set; }
        public Project Project { get; set; }

        public List<ResourcesMatch> ResourceMatches { get; set; }
    }
}