﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Atoz.Web.Models.Atoz
{
    public class AtozTag
    {
        [Key]
        public string Text { get; set; }
        public List<Project> Projects { get; set; }
        public List<Resource> Resources { get; set; } 
    }
}