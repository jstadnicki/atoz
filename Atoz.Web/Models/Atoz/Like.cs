﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Atoz.Web.Models.Atoz
{
    public class Like
    {
        [Key, Column(Order = 0)]
        public string UserId { get; set; }
        [Key, Column(Order = 1)]
        public int ProjectId { get; set; }
        public ApplicationUser User { get; set; }
        public Project Project { get; set; }
    }
}