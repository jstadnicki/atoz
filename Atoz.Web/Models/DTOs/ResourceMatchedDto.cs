﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Atoz.Web.Models.DTOs
{
    public class ResourceMatchedDto
    {
        public int ResourceId { get; set; }
        public string ResourceName { get; set; }
        public List<ProjectDto> Projects { get; set; }
    }
}