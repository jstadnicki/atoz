﻿using System.ComponentModel.DataAnnotations;

namespace Atoz.Web.Models.DTOs
{
    public class HomeDto
    {
        [Required]
        public string ProjectTags { get; set; }
    }
}