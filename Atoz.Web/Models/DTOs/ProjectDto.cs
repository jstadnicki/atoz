﻿namespace Atoz.Web.Models.DTOs
{
    public class ProjectDto
    {
        public int ProjectId { get; set; }
        public string ProjectName { get; set; }
        public int RequiredResourceId { get; internal set; }
        public string RequiredResourceName { get; internal set; }
    }
}