﻿namespace Atoz.Web.Models.DTOs
{
    public class RequiredResourcesDto
    {
        public string Description { get; set; }
        public int Amount { get; set; }
    }
}