﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Atoz.Web.Models.DTOs
{
    public class ProjectCreateDto
    {
        [Required]
        public string Title { get; set; }

        [Required]
        public string Description { get; set; }

        public string Tags { get; set; }

        public List<RequiredResourcesDto> RequiredResources { get; set; }
    }
}