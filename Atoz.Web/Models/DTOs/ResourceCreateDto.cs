﻿using DataAnnotationsExtensions;
using System.ComponentModel.DataAnnotations;

namespace Atoz.Web.Models.DTOs
{
    public class ResourceCreateDto
    {
        public ResourceCreateDto()
        {
            Amount = 1;
        }

        [Required]
        public string Name { get; set; }
        [Required]
        public string Description { get; set; }
        [Min(1)]
        public int Amount { get; set; }

        public string Tags { get; set; }
    }
}