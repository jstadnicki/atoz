using System.Data.Entity;
using Atoz.Web.Models.Atoz;
using Microsoft.AspNet.Identity.EntityFramework;

namespace Atoz.Web.Models
{
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext()
            : base("DefaultConnection", throwIfV1Schema: false)
        {
        }

        public static ApplicationDbContext Create()
        {
            return new ApplicationDbContext();
        }

        public DbSet<Project> Projects { get; set; }
        public DbSet<AtozTag> Tags { get; set; }
        public DbSet<Resource> Resources { get; set; }
        public DbSet<ResourcesMatch> ResourcesMatches { get; set; }
        public DbSet<Like> Likes { get; set; }
        public DbSet<Dislike> Dislikes { get; set; }
    }
}