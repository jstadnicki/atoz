﻿using Atoz.Web.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Atoz.Web.Models.ViewModels
{
    public class ProjectListItemViewModel
    {
        public string Title { get; set; }
        public List<RequiredResourceDescription> RequiredResources { get; set; }
    }
}
