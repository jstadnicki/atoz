﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Atoz.Web.Models.ViewModels
{
    public class ResourceDetailsViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public int Amount { get; set; }
        public DateTime Created { get; set; }
        public DateTime Modified { get; set; }
        public List<string> Tags { get; set; }
        public bool IsOwner { get; set; }

        public ProjectsListViewModel RequiredRecourses { get; set; }
    }

    public class ResourceToAcceptViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public List<ResourceToAcceptViewModel> ResourceModel { get; set; }
    }
}