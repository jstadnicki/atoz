using System.Collections.Generic;
using Atoz.Web.Repositories;

namespace Atoz.Web.Models.ViewModels
{
    public class ResourcesByTagViewModel
    {
        public List<ResourceName> Resources { get; set; }
        public string Tag { get; set; }
    }
}