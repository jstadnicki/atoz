﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Atoz.Web.Models.ViewModels
{
    public class ProjectHomeIndexViewModel
    {
        public string Title { get; set; }
        public string Description { get; set; }
        public int Id { get; set; }
    }
}