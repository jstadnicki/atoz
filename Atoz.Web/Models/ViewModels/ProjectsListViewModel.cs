﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Atoz.Web.Models.ViewModels
{
    public class ProjectsListViewModel
    {
        public List<ProjectListItemViewModel> Projects { get; set; }
    }
}