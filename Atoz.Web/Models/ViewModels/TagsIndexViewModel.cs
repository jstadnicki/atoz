using Atoz.Web.Repositories;

namespace Atoz.Web.Models.ViewModels
{
    public class TagsIndexViewModel
    {
        public TopTags TopTags { get; set; }
    }
}