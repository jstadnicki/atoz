﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Atoz.Web.Models.ViewModels
{
    public class MyProjectActionViewModel
    {
        public int ProjectID { get; set; }
        public string ProjectName { get; set; }
        public List<ResourceToAcceptViewModel> ResourceDetails { get; set; }

    }

    public class MyResourceActionViewModel
    {
        public int ResourceId { get; set; }
        public string ResourceName { get; set; }
        public List<ProjectToAcceptViewModel> ProjectDetails { get; set; }
    }

}