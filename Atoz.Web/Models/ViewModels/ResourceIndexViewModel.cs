﻿using System.Collections.Generic;
using Atoz.Web.Repositories;

namespace Atoz.Web.Models.ViewModels
{
    public class ResourceIndexViewModel
    {
        public List<ResourceName> Resources { get; set; }
    }
}