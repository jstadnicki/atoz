﻿using System.Collections.Generic;
using Atoz.Web.Repositories;

namespace Atoz.Web.Models.ViewModels
{
    public class HomeIndexViewModel
    {
        public List<ProjectHomeIndexViewModel> Projects { get; set; }
    }
}