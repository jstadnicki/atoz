﻿namespace Atoz.Web.Models.ViewModels
{
    public class ProjectToAcceptViewModel
    {
        public int Id { get; set; }
        public string ProjectName { get; set; }
        public int ResourceId { get; set; }
        public string ResourceName { get; set; }
    }

}