﻿using System.Collections.Generic;
using Atoz.Web.Models.DTOs;

namespace Atoz.Web.Models.ViewModels
{
    public class ProjectCreateViewModel
    {
        public ProjectCreateViewModel()
        {
            this.RequiredResources = new List<RequiredResourcesDto>
            {
                new RequiredResourcesDto(),
                new RequiredResourcesDto(),
                new RequiredResourcesDto()
            };
        }
        public string Title { get; set; }
        public string Description { get; set; }
        public string Tags { get; set; }
        public List<RequiredResourcesDto> RequiredResources { get; set; }
    }
}