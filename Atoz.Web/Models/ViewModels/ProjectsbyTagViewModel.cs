using System.Collections.Generic;
using Atoz.Web.Repositories;

namespace Atoz.Web.Models.ViewModels
{
    public class ProjectsByTagViewModel
    {
        public List<ProjectTitle> Projects { get; set; }
        public string Tag { get; set; }
    }
}