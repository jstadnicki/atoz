﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Atoz.Web.Models.ViewModels
{
    public class ResourceCreateViewModel
    {
        public ResourceCreateViewModel()
        {
            Amount = 1;
        }

        public string Name { get; set; }
        public string Description { get; set; }
        public int Amount { get; set; }
        public string Tags { get; set; }
    }
}