﻿namespace Atoz.Web.Models.ViewModels
{
    public class LikeDislikeViewModel
    {
        public int ProjectId { get; set; }
        public int LikeCount { get; set; }
        public bool HasUserAlreadyLiked { get; set; }
        public int DislikeCount { get; set; }
        public bool HasUserAlreadyDisliked { get; set; }
        //public bool CanLikeOrDislike
        //{
        //    get { return !HasUserAlreadyLiked && !HasUserAlreadyDisliked; }
        //}
    }
}