using System.Collections.Generic;
using Atoz.Web.Repositories;

namespace Atoz.Web.Models.ViewModels
{
    public class ProjectsIndexViewModel
    {
        public List<ProjectTitle> Projects { get; set; }
    }
}