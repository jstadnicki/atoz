﻿using System;
using System.Linq;
using System.Collections.Generic;
using Atoz.Web.Services;

namespace Atoz.Web.Models.ViewModels
{
    public class ProjectDetailsViewModel
    {
        public string Authorname { get; set; }
        public string Description { get; set; }
        public object Id { get; set; }
        public bool IsOwner { get; set; }
        public List<string> Tags { get; set; }
        public string Title { get; set; }
        public DateTime Created { get; set; }
        public DateTime Modified { get; set; }
        public List<RequiredResourceViewModel> RequiredResources { get; set; }
        public bool RequirementsFulfilled { get; set; }
        public LikeDislikeViewModel LikeDislike { get; set; }
        public string ShortFacebookDescription
        {
            get
            {
                if (Description.Length > 270)
                {

                    return System.Web.HttpUtility.HtmlEncode(Description.Replace("\r\n", " ").Substring(0, 270));
                }

                return System.Web.HttpUtility.HtmlEncode(Description.Replace("\r\n", string.Empty));
            }
        }

        public List<ResourceNameViewModel> CurrentUserAvailableResources { get; set; }
    }
}