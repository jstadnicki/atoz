﻿using System.Linq;
using Atoz.Web.Models.Atoz;
using Atoz.Web.Models.DTOs;
using Atoz.Web.Models.ViewModels;
using AutoMapper;

namespace Atoz.Web.Mappings
{
    public class ResourcesMappings
    {
        public static void Register()
        {
            Mapper.CreateMap<ResourceCreateDto, Resource>()
                .ForMember(dest => dest.Tags, opts => opts.Ignore());
            Mapper.CreateMap<Resource, ResourceCreateViewModel>();
            Mapper.CreateMap<Resource, ResourceDetailsViewModel>()
                .ForMember(dest => dest.Tags, opts => opts.MapFrom(src => src.Tags.Select(t => t.Text)));
            Mapper.CreateMap<ResourceCreateDto, ResourceCreateViewModel>();
        }
    }
}