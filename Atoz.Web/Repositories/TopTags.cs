﻿using System.Collections.Generic;

namespace Atoz.Web.Repositories
{
    public class TopTags
    {
        public List<string> ProjectTags { get; set; } 
        public List<string> ResourceTags { get; set; } 
    }
}