using System.Collections.Generic;
using Atoz.Web.Models.Atoz;

namespace Atoz.Web.Repositories
{
    public interface ITagsRepository
    {
        List<AtozTag> Get(string prefix);
        List<AtozTag> GetByText(List<string> tagsText);
        TopTags GetTop(int take = 10);
    }
}