﻿using System.Collections.Generic;
using Atoz.Web.Models.Atoz;
using Atoz.Web.Models.ViewModels;

namespace Atoz.Web.Repositories
{
    public interface IProjectsRepository
    {
        void CreateAndSaveNewProject(Project project);
        List<ProjectTitle> LoadAllProjectsTitles();
        Project GetProjectDetails(int id);
        List<Project> GetProjectsForUser(string userId);
        List<ProjectTitle> GetProjectsByTag(string tag);
        List<Project> GetUserProjects(string userId);
        List<ProjectHomeIndexViewModel> GetRandomProjects();
        List<ProjectTitle> SearchByTags(List<AtozTag> tags);
        void Like(int id, string userId);
        void Dislike(int id, string userId);
    }
}