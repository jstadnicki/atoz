﻿using Atoz.Web.Models.Atoz;
using Atoz.Web.Models.DTOs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Atoz.Web.Repositories
{
    public interface IResourcesRepository
    {
        void CreateAndSaveNewResource(Resource resource);
        List<ResourceName> GetResourceNames();
        Resource GetResourceDetails(int id);
        List<ResourceName> GetAllResourcesNamesForUser(string userId);
        List<ResourceName> GetResourcesByTag(string tag);
        List<ResourceMatchedDto> GetResourceForUser(string userId);
    }
}
