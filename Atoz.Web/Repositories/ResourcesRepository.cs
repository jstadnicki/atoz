﻿using Atoz.Web.Models;
using Atoz.Web.Models.Atoz;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Entity;
using Atoz.Web.Models.DTOs;

namespace Atoz.Web.Repositories
{
    public class ResourcesRepository : IResourcesRepository
    {
        private readonly ApplicationDbContext database;

        public ResourcesRepository(ApplicationDbContext database)
        {
            this.database = database;
        }

        public void CreateAndSaveNewResource(Resource resource)
        {
            database.Resources.Add(resource);
            database.SaveChanges();
        }

        public List<ResourceName> GetResourceNames()
        {
            return database
                .Resources
                .Select(p => new ResourceName { Id = p.Id, Name = p.Name })
                .ToList(); ;
        }

        public Resource GetResourceDetails(int id)
        {
            return this.database.Resources
                .Include(x => x.Tags)
                .Include(x => x.User)
                .Single(x => x.Id == id);
        }

        public List<ResourceName> GetAllResourcesNamesForUser(string userId)
        {
            return this.database.Resources
                .Where(r => r.UserId == userId)
                .Select(r => new ResourceName { Id = r.Id, Name = r.Name })
                .ToList();
        }

        public List<ResourceName> GetResourcesByTag(string tag)
        {
            return this.database.Resources
                .Where(p => p.Tags.Any(t => t.Text == tag))
                .Select(r => new ResourceName { Id = r.Id, Name = r.Name })
                .ToList();
        }

        public List<ResourceMatchedDto> GetResourceForUser(string userId)
        {
            return this.database.Resources
                .Where(r=> r.User.Id == userId)
                .Where(r =>
                r.RequiredResourceMatches.Any(rrm => rrm.Status == ResourcesMatchStatus.Offered)
                && r.RequiredResourceMatches.All(rrm => rrm.Status != ResourcesMatchStatus.Accepted)
                )
                .Select(r => new ResourceMatchedDto
                {
                    ResourceId = r.Id,
                    ResourceName = r.Name,
                    Projects = r.RequiredResourceMatches
                    .Where(rrm => rrm.Status == ResourcesMatchStatus.Offered)
                    .Select(rrm => new ProjectDto
                    {
                        ProjectId = rrm.RequiredResource.Project.Id,
                        ProjectName = rrm.RequiredResource.Project.Title,
                        RequiredResourceId = rrm.RequiredResourceId,
                        RequiredResourceName = rrm.RequiredResource.Description
                    }).ToList()
                }).ToList();
        }
    }
}