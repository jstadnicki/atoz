﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Atoz.Web.Repositories
{
    public class RequiredResourceDescription
    {
        public int Id { get; set; }
        public string Description { get; set; }
    }
}