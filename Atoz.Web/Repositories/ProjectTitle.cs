namespace Atoz.Web.Repositories
{
    public class ProjectTitle
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public int RequirementsCount { get; set; }
        public int AcceptedRequirementsCount { get; set; }
        public int NotAcceptedRequirementsCount { get { return RequirementsCount - AcceptedRequirementsCount; } }

    }
}