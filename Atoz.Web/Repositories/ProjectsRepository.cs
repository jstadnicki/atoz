﻿using System.Collections.Generic;
using System.Linq;
using System.Data.Entity;
using Atoz.Web.Models;
using Atoz.Web.Models.Atoz;
using System;
using Atoz.Web.Models.ViewModels;

namespace Atoz.Web.Repositories
{
    public class ProjectsRepository : IProjectsRepository
    {
        private readonly ApplicationDbContext database;

        public ProjectsRepository(ApplicationDbContext database)
        {
            this.database = database;
        }

        public void CreateAndSaveNewProject(Project project)
        {
            this.database.Projects.Add(project);
            this.database.SaveChanges();
        }

        public List<ProjectTitle> LoadAllProjectsTitles()
        {
            return this.database
                .Projects
                .Select(p => new ProjectTitle {
                    Id = p.Id, 
                    Title = p.Title,
                    RequirementsCount = p.RequiredResources.Count,
                    AcceptedRequirementsCount = p.RequiredResources.Count(rr => rr.ResourceMatches.Any(rm => rm.Status == ResourcesMatchStatus.Accepted))
                })
                .ToList();
        }

        public Project GetProjectDetails(int id)
        {
            return this.database.Projects
                .Include(x => x.Tags)
                .Include(x => x.RequiredResources)
                .Include(x => x.RequiredResources.Select(rr => rr.ResourceMatches))
                .Include(x => x.RequiredResources.Select(rr => rr.ResourceMatches.Select(rm => rm.Resource)))
                .Include(x => x.User)
                .Include(x => x.Likes)
                .Include(x => x.Dislikes)
                .Single(x => x.Id == id);
        }

        public List<Project> GetProjectsForUser(string userId)
        {
            return this.database.Projects
                .Include(x => x.Tags)
                .Include(x => x.RequiredResources)
                .Include(x => x.RequiredResources.Select(rr => rr.ResourceMatches))
                .Include(x => x.RequiredResources.Select(rr => rr.ResourceMatches.Select(rm=> rm.Resource)))
                .Include(x => x.User)
                .Where(x => x.UserId == userId && x.RequiredResources.Any(rr=> rr.ResourceMatches.Where(rm=> rm.Status == ResourcesMatchStatus.Offered).Count() > 0 && rr.ResourceMatches.All(rm => rm.Status != ResourcesMatchStatus.Accepted)) )
                .ToList();
        }

        public List<ProjectTitle> GetProjectsByTag(string tag)
        {
            return this.database.Projects
                .Where(p => p.Tags.Any(t => t.Text == tag))
                .Select(p => new ProjectTitle { Id = p.Id, Title = p.Title })
                .ToList();
        }
        
        public List<Project> GetUserProjects(string userId)
        {
            return database.Projects
                .Where(p => p.RequiredResources.Any() && p.UserId == userId)
                .Include(p => p.RequiredResources)
                .ToList();
        }

        public List<ProjectHomeIndexViewModel> GetRandomProjects()
        {
            var max = this.database.Projects.Count();
            var rand = new Random(DateTime.Now.Second);
            var randomIDList = new int[] { rand.Next(max), rand.Next(max), rand.Next(max) };

            var projects = this.database.Projects.ToList();

            List<Project> randomList = new List<Project>
            {
                projects.ElementAt(rand.Next(max)),
                projects.ElementAt(rand.Next(max)),
                projects.ElementAt(rand.Next(max))
            };

            return randomList.Select(p => new ProjectHomeIndexViewModel
                {
                    Description = p.Description,
                    Id = p.Id,
                    Title = p.Title
                }).ToList();
        }

        public List<ProjectTitle> SearchByTags(List<AtozTag> tags)
        {
            var tagsText = tags.Select(t => t.Text).ToList();

            return this.database.Projects
                .Where(p => p.Tags.Any(t => tagsText.Contains(t.Text)))
                .Select(p => new ProjectTitle { Id = p.Id, Title = p.Title })
                .ToList();
        }

        public void Like(int id, string userId)
        {
            var dis = database.Dislikes.SingleOrDefault(d => d.ProjectId == id && d.UserId == userId);
            if (dis != null)
            {
                database.Dislikes.Remove(dis);
            }

            var like = database.Likes.SingleOrDefault(d => d.ProjectId == id && d.UserId == userId);
            if (like != null)
            {
                return;
            }

            database.Likes.Add(new Like
            {
                ProjectId = id,
                UserId = userId
            });
            database.SaveChanges();
        }

        public void Dislike(int id, string userId)
        {
            var like = database.Likes.SingleOrDefault(d => d.ProjectId == id && d.UserId == userId);
            if (like != null)
            {
                database.Likes.Remove(like);
            }

            var dis = database.Dislikes.SingleOrDefault(d => d.ProjectId == id && d.UserId == userId);
            if (dis != null)
            {
                return;
            }

            database.Dislikes.Add(new Dislike
            {
                ProjectId = id,
                UserId = userId
            });
            database.SaveChanges();
        }
    }
}