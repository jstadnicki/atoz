﻿using System.Collections.Generic;
using System.Linq;
using Atoz.Web.Models;
using Atoz.Web.Models.Atoz;

namespace Atoz.Web.Repositories
{
    public class TagsRepository : ITagsRepository
    {
        private readonly ApplicationDbContext database;

        public TagsRepository(ApplicationDbContext database)
        {
            this.database = database;
        }

        public List<AtozTag> Get(string prefix)
        {
            return this.database
                .Tags
                .Where(t => t.Text.StartsWith(prefix))
                .Take(20)
                .ToList();
        }

        public List<AtozTag> GetByText(List<string> tagsText)
        {
            return this.database.Tags
                .Where(pt => tagsText.Contains(pt.Text))
                .ToList();
        }

        public TopTags GetTop(int take = 10)
        {
            var projectTags = (from tag in this.database.Tags
                select new
                {
                    tag,
                    tag.Projects.Count
                })
                .OrderByDescending(x => x.Count)
                .Take(take)
                .Select(x => x.tag.Text)
                .ToList();

            var resourceTags = (from tag in this.database.Tags
                               select new
                               {
                                   tag,
                                   tag.Resources.Count
                               })
                .OrderByDescending(x => x.Count)
                .Take(take)
                .Select(x => x.tag.Text)
                .ToList();

            return new TopTags
            {
                ProjectTags = projectTags,
                ResourceTags = resourceTags
            };
        }
    }
}