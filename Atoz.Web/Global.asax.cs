﻿using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using Autofac;
using System;
using Atoz.Web.Mappings;

namespace Atoz.Web
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            AtozAutofac.RegisterDependencies();
            AllMappings.Register();
        }

        protected virtual void Application_Error(object sender, EventArgs e)
        {
            var exception = Server.GetLastError();
        }
    }
}
