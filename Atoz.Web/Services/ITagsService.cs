using System.Collections.Generic;
using Atoz.Web.Models.Atoz;
using Atoz.Web.Models.ViewModels;

namespace Atoz.Web.Services
{
    public interface ITagsService
    {
        List<string> Get(string prefix);
        List<AtozTag> GetByText(string tags);
        TagsIndexViewModel GetTop(int take = 10);
    }
}