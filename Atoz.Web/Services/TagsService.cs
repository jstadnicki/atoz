﻿using System;
using System.Collections.Generic;
using System.Linq;
using Atoz.Web.Models.Atoz;
using Atoz.Web.Models.ViewModels;
using Atoz.Web.Repositories;

namespace Atoz.Web.Services
{
    public class TagsService : ITagsService
    {
        private readonly ITagsRepository repository;

        public TagsService(ITagsRepository repository)
        {
            this.repository = repository;
        }

        public List<string> Get(string prefix)
        {
            return repository.Get(prefix)
                .Select(t => t.Text)
                .ToList();
        }

        public List<AtozTag> GetByText(string tags)
        {
            if (string.IsNullOrWhiteSpace(tags))
            {
                return new List<AtozTag>(0);
            }

            var tagstext = tags.Split(new[] { ";" }, StringSplitOptions.RemoveEmptyEntries).ToList();
            var existingtags = repository.GetByText(tagstext);
            existingtags.ForEach(t => tagstext.Remove(t.Text));
            existingtags.AddRange(tagstext.Select(t => new AtozTag { Text = t }));
            return existingtags;
        }

        public TagsIndexViewModel GetTop(int take = 10)
        {
            return new TagsIndexViewModel
            {
                TopTags = repository.GetTop(take)
            };
        }
    }
}