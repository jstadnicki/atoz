﻿using Atoz.Web.Models.Atoz;
using Atoz.Web.Models.DTOs;
using Atoz.Web.Repositories;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.AspNet.Identity;
using Atoz.Web.Models.ViewModels;

namespace Atoz.Web.Services
{
    public class ResourcesService : IResourcesService
    {
        private readonly IResourcesRepository resourcesRepository;
        private readonly IProjectsRepository projectsRepository;
        private ITagsService tagsService;

        public ResourcesService(
            IResourcesRepository resourcesRepository,
            IProjectsRepository projectsRepository, 
            ITagsService tagsService)
        {
            this.resourcesRepository = resourcesRepository;
            this.projectsRepository = projectsRepository;
            this.tagsService = tagsService;
        }

        public void CreateResource(ResourceCreateDto model)
        {
            var resource = Mapper.Map<Resource>(model);
            resource.UserId = HttpContext.Current.User.Identity.GetUserId();
            resource.Created = DateTime.Now;
            resource.Modified = DateTime.Now;
            
            resource.Tags = tagsService.GetByText(model.Tags);

            resourcesRepository.CreateAndSaveNewResource(resource);
        }

        public ResourceIndexViewModel GetResourcesIndexViewModel()
        {
            var resourcesNames = this.resourcesRepository.GetResourceNames();
            return new ResourceIndexViewModel { Resources = resourcesNames };
        }

        public ResourceDetailsViewModel GetResourceDetailsViewModel(int id)
        {
            var model = this.resourcesRepository.GetResourceDetails(id);
            var userId = HttpContext.Current.User.Identity.GetUserId();

            var resourceDetails = Mapper.Map<ResourceDetailsViewModel>(model);
            resourceDetails.IsOwner = model.UserId == userId;
            resourceDetails.RequiredRecourses = GetRequiredResources();
            
            return resourceDetails;
        }

        private ProjectsListViewModel GetRequiredResources()
        {
            var userId = HttpContext.Current.User.Identity.GetUserId();

            var projects = projectsRepository.GetUserProjects(userId);

            var model = new ProjectsListViewModel
            {
                Projects = projects.Select(p => new ProjectListItemViewModel
                {
                    Title = p.Title,
                    RequiredResources = p.RequiredResources.Select(rr => new RequiredResourceDescription
                    {
                        Description = rr.Description,
                        Id = rr.Id
                    }).ToList()
                }).ToList()
            };

            return model;
        }

        public ResourcesByTagViewModel GetResourcesByTag(string tag)
        {
            var tagResources = this.resourcesRepository.GetResourcesByTag(tag);
            return new ResourcesByTagViewModel { Resources = tagResources, Tag = tag };
        }
    }
}