﻿using System;
using System.Linq;
using System.Web;
using Atoz.Web.Models.Atoz;
using Atoz.Web.Models.DTOs;
using Atoz.Web.Models.ViewModels;
using Atoz.Web.Repositories;
using Microsoft.AspNet.Identity;
using System.Collections.Generic;
using Atoz.Web.Controllers;

namespace Atoz.Web.Services
{
    public class ProjectsService : IProjectsService
    {
        private readonly IProjectsRepository projectsRepository;
        private readonly ITagsService tagsService;
        private readonly IResourcesRepository resourcesRepository;

        public ProjectsService(IProjectsRepository projectsRepository, ITagsService tagsService, IResourcesRepository resourcesRepository)
        {
            this.projectsRepository = projectsRepository;
            this.tagsService = tagsService;
            this.resourcesRepository = resourcesRepository;
        }

        public void CreateProject(ProjectCreateDto model)
        {
            var currentUserId = HttpContext.Current.User.Identity.GetUserId();
            var tags = tagsService.GetByText(model.Tags);

            var project = new Project
            {
                Created = DateTime.Now,
                Description = model.Description,
                Title = model.Title,
                UserId = currentUserId,
                Modified = DateTime.Now,
                Tags = tags
            };

            project.RequiredResources = model.RequiredResources
                .Where(rr => !string.IsNullOrEmpty(rr.Description))
                .Select(rr =>
                new RequiredResource
                {
                    Project = project,
                    Description = rr.Description,
                    Amount = rr.Amount
                }).ToList();

            this.projectsRepository.CreateAndSaveNewProject(project);
        }

        public ProjectsIndexViewModel GetProjectsIndexViewModel()
        {
            var allProjectsTitles = this.projectsRepository.LoadAllProjectsTitles();
            return new ProjectsIndexViewModel { Projects = allProjectsTitles };
        }

        public ProjectsByTagViewModel GetProjectsByTag(string tag)
        {
            var tagProjects = this.projectsRepository.GetProjectsByTag(tag);
            return new ProjectsByTagViewModel { Projects = tagProjects, Tag = tag };
        }

        public HomeIndexViewModel GetHomeIndexViewModel()
        {
            var randomProjects = this.projectsRepository.GetRandomProjects();
            
            return new HomeIndexViewModel
            {
                Projects = randomProjects,
            };
        }

        public ProjectsSearchViewModel GetProjectsSearchViewModel(HomeDto dto)
        {
            List<ProjectTitle> searchProjects = new List<ProjectTitle>(0);
            if (dto != null)
            {
                var tags = tagsService.GetByText(dto.ProjectTags);
                searchProjects = projectsRepository.SearchByTags(tags);
            }

            return new ProjectsSearchViewModel
            {
                SearchProjects = searchProjects
            };
        }

        public ProjectDetailsViewModel GetProjectDetailsViewModel(int id)
        {
            var model = this.projectsRepository.GetProjectDetails(id);
            var userId = HttpContext.Current.User.Identity.GetUserId();

            var projectDetailsViewModel = new ProjectDetailsViewModel
            {
                Id = model.Id,
                Title = model.Title,
                Description = model.Description,
                Tags = model.Tags.Select(t => t.Text).ToList(),
                Authorname = model.User.UserName,
                IsOwner = model.UserId == userId,
                Created = model.Created,
                Modified = model.Modified,
                LikeDislike = new LikeDislikeViewModel
                {
                    ProjectId = model.Id,
                    LikeCount = model.Likes.Count,
                    HasUserAlreadyLiked = model.Likes.Any(l => l.UserId == userId),
                    DislikeCount = model.Dislikes.Count,
                    HasUserAlreadyDisliked = model.Dislikes.Any(l => l.UserId == userId),
                },
                RequirementsFulfilled = model.RequiredResources.Any() && model.RequiredResources.All(rr => rr.ResourceMatches.Any(rm => rm.Status == ResourcesMatchStatus.Accepted)),
                RequiredResources = model.RequiredResources.Select(rr => new RequiredResourceViewModel
                {
                    Description = rr.Description,
                    Amount = rr.Amount,
                    Id = rr.Id,
                    Accepted = rr.ResourceMatches.Any(rm => rm.Status == ResourcesMatchStatus.Accepted)
                }).ToList(),
                CurrentUserAvailableResources = this.resourcesRepository
                    .GetAllResourcesNamesForUser(userId)
                    .Select(r => new ResourceNameViewModel { Id = r.Id, Name = r.Name }).ToList(),
            };

            return projectDetailsViewModel;
        }

        public List<MyProjectActionViewModel> GetMyProjects(string userId)
        {
            var model = this.projectsRepository.GetProjectsForUser(userId);
            return model.Select(p => new MyProjectActionViewModel
            {
                ProjectID = p.Id,
                ProjectName = p.Title,
                ResourceDetails = p.RequiredResources.Where(rr => rr.ResourceMatches.Any(rm => rm.Status == ResourcesMatchStatus.Offered)).Select(rr => new ResourceToAcceptViewModel
                {
                    Id = rr.Id,
                    Name = rr.Description,
                    ResourceModel = rr.ResourceMatches.Where(rm => rm.Status == ResourcesMatchStatus.Offered).Select(rm => new ResourceToAcceptViewModel
                    {
                        Id = rm.ResourceId,
                        Name = rm.Resource.Name.ToString()
                    }).ToList()
                }).ToList()

            }).ToList();
        }

        public List<MyResourceActionViewModel> GetMyResources(string userId)
        {
            var model = this.resourcesRepository.GetResourceForUser(userId);
            return model.Select(p => new MyResourceActionViewModel
            {
                ResourceName = p.ResourceName,
                ResourceId = p.ResourceId,
                ProjectDetails = p.Projects.Select(r=> new ProjectToAcceptViewModel
                {
                    Id = r.ProjectId,
                    ProjectName = r.ProjectName,
                    ResourceId = r.RequiredResourceId,
                    ResourceName = r.RequiredResourceName
                }).ToList()
            }).ToList();
        }

        public void Like(int id)
        {
            var userId = HttpContext.Current.User.Identity.GetUserId();
            projectsRepository.Like(id, userId);
        }

        public void Dislike(int id)
        {
            var userId = HttpContext.Current.User.Identity.GetUserId();
            projectsRepository.Dislike(id, userId);
        }
    }

    public class ResourceNameViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }

    public class RequiredResourceViewModel
    {
        public string Description { get; set; }
        public int Amount { get; set; }
        public int Id { get; set; }
        public bool Accepted { get; set; }
    }
}