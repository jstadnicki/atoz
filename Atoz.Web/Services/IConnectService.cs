namespace Atoz.Web.Services
{
    public interface IConnectService
    {
        void SponsorResource(int requiredResourceId, int sponsoredResourceId);
        void RequestResource(int sponsoredResourceId, int requiredResourceId);
        void Accept(int requiredResourceId, int sponsoredResourceId);
    }
}