using System;
using System.Linq;
using Atoz.Web.Models;
using Atoz.Web.Models.Atoz;

namespace Atoz.Web.Services
{
    public class ConnectService : IConnectService
    {
        private readonly ApplicationDbContext database;

        public ConnectService(ApplicationDbContext database)
        {
            this.database = database;
        }

        public void SponsorResource(int requiredResourceId, int sponsoredResourceId)
        {
            var resourcesMatch = CreateMatch(sponsoredResourceId, requiredResourceId, ResourcesMatchStatus.Offered);
            this.database.ResourcesMatches.Add(resourcesMatch);
            this.database.SaveChanges();
        }

        public void RequestResource(int sponsoredResourceId, int requiredResourceId)
        {
            var resourcesMatch = CreateMatch(sponsoredResourceId, requiredResourceId, ResourcesMatchStatus.Required);
            this.database.ResourcesMatches.Add(resourcesMatch);
            this.database.SaveChanges();
        }

        public void Accept(int requiredResourceId, int sponsoredResourceId)
        {
            this.database.ResourcesMatches
                .Where(m=>m.RequiredResourceId==requiredResourceId)
                .Where(m=>m.ResourceId == sponsoredResourceId)
                .Single()
                .Status=ResourcesMatchStatus.Accepted;
            this.database.SaveChanges();
        }

        private static ResourcesMatch CreateMatch(int sponsoredResourceId, int requiredResourceId, ResourcesMatchStatus required)
        {
            var resourcesMatch = new ResourcesMatch
            {
                RequiredResourceId = requiredResourceId,
                ResourceId = sponsoredResourceId,
                Status = required,
                CreateDate = DateTime.Now,
            };
            return resourcesMatch;
        }
    }
}