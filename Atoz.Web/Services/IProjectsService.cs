﻿using Atoz.Web.Models.DTOs;
using Atoz.Web.Models.ViewModels;
using System.Collections.Generic;
using Atoz.Web.Controllers;

namespace Atoz.Web.Services
{
    public interface IProjectsService
    {
        void CreateProject(ProjectCreateDto model);
        ProjectsIndexViewModel GetProjectsIndexViewModel();
        ProjectDetailsViewModel GetProjectDetailsViewModel(int id);
        ProjectsByTagViewModel GetProjectsByTag(string tag);
        List<MyProjectActionViewModel> GetMyProjects(string userId);
        ProjectsSearchViewModel GetProjectsSearchViewModel(HomeDto dto);
        HomeIndexViewModel GetHomeIndexViewModel();
        List<MyResourceActionViewModel> GetMyResources(string userId);
        void Like(int id);
        void Dislike(int id);
    }
}