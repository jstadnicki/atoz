﻿using Atoz.Web.Models.DTOs;
using Atoz.Web.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Atoz.Web.Services
{
    public interface IResourcesService
    {
        void CreateResource(ResourceCreateDto model);
        ResourceIndexViewModel GetResourcesIndexViewModel();
        ResourceDetailsViewModel GetResourceDetailsViewModel(int id);
        ResourcesByTagViewModel GetResourcesByTag(string tag);
    }
}
