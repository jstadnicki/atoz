﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Atoz.Web.Startup))]
namespace Atoz.Web
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
